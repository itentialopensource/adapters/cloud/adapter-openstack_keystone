
## 0.3.4 [10-15-2024]

* Changes made at 2024.10.14_19:57PM

See merge request itentialopensource/adapters/adapter-openstack_keystone!13

---

## 0.3.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-openstack_keystone!11

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_18:05PM

See merge request itentialopensource/adapters/adapter-openstack_keystone!10

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_19:18PM

See merge request itentialopensource/adapters/adapter-openstack_keystone!9

---

## 0.3.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-openstack_keystone!8

---

## 0.2.5 [03-28-2024]

* Changes made at 2024.03.28_13:12PM

See merge request itentialopensource/adapters/cloud/adapter-openstack_keystone!6

---

## 0.2.4 [03-21-2024]

* Changes made at 2024.03.21_13:49PM

See merge request itentialopensource/adapters/cloud/adapter-openstack_keystone!5

---

## 0.2.3 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-openstack_keystone!4

---

## 0.2.2 [03-11-2024]

* Changes made at 2024.03.11_15:30PM

See merge request itentialopensource/adapters/cloud/adapter-openstack_keystone!3

---

## 0.2.1 [02-28-2024]

* Changes made at 2024.02.28_11:41AM

See merge request itentialopensource/adapters/cloud/adapter-openstack_keystone!2

---

## 0.2.0 [12-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-openstack_keystone!1

---

## 0.1.2 [12-11-2023]

* Bug fixes and performance improvements

See commit 1f89abb

---

## 0.1.1 [08-31-2022]

* Bug fixes and performance improvements

See commit 2b2eb51

---
