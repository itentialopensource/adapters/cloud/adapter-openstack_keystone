## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for OpenStack Keystone. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for OpenStack Keystone.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Openstack_keystone. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">validateAndShowInformationForToken(nocatalog, allowExpired, callback)</td>
    <td style="padding:15px">Validate and show information for token</td>
    <td style="padding:15px">{base_path}/{version}/v3/auth/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkToken(allowExpired, callback)</td>
    <td style="padding:15px">Check token</td>
    <td style="padding:15px">{base_path}/{version}/v3/auth/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeToken(callback)</td>
    <td style="padding:15px">Revoke token</td>
    <td style="padding:15px">{base_path}/{version}/v3/auth/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceCatalog(callback)</td>
    <td style="padding:15px">Get service catalog</td>
    <td style="padding:15px">{base_path}/{version}/v3/auth/catalog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvailableProjectScopes(callback)</td>
    <td style="padding:15px">Get available project scopes</td>
    <td style="padding:15px">{base_path}/{version}/v3/auth/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvailableDomainScopes(callback)</td>
    <td style="padding:15px">Get available domain scopes</td>
    <td style="padding:15px">{base_path}/{version}/v3/auth/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvailableSystemScopes(callback)</td>
    <td style="padding:15px">Get available system scopes</td>
    <td style="padding:15px">{base_path}/{version}/v3/auth/system?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplicationCredential(userId, body, callback)</td>
    <td style="padding:15px">Create application credential</td>
    <td style="padding:15px">{base_path}/{version}/v3/users/{pathv1}/application_credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listApplicationCredentials(name, userId, callback)</td>
    <td style="padding:15px">List application credentials</td>
    <td style="padding:15px">{base_path}/{version}/v3/users/{pathv1}/application_credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showApplicationCredentialDetails(userId, applicationCredentialId, callback)</td>
    <td style="padding:15px">Show application credential details</td>
    <td style="padding:15px">{base_path}/{version}/v3/users/{pathv1}/application_credentials/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplicationCredential(userId, applicationCredentialId, callback)</td>
    <td style="padding:15px">Delete application credential</td>
    <td style="padding:15px">{base_path}/{version}/v3/users/{pathv1}/application_credentials/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccessRules(userId, callback)</td>
    <td style="padding:15px">List access rules</td>
    <td style="padding:15px">{base_path}/{version}/v3/users/{pathv1}/access_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAccessRuleDetails(userId, accessRuleId, callback)</td>
    <td style="padding:15px">Show access rule details</td>
    <td style="padding:15px">{base_path}/{version}/v3/users/{pathv1}/access_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccessRule(userId, accessRuleId, callback)</td>
    <td style="padding:15px">Delete access rule</td>
    <td style="padding:15px">{base_path}/{version}/v3/users/{pathv1}/access_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCredential(body, callback)</td>
    <td style="padding:15px">Create credential</td>
    <td style="padding:15px">{base_path}/{version}/v3/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCredentials(userId, callback)</td>
    <td style="padding:15px">List credentials</td>
    <td style="padding:15px">{base_path}/{version}/v3/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showCredentialDetails(credentialId, callback)</td>
    <td style="padding:15px">Show credential details</td>
    <td style="padding:15px">{base_path}/{version}/v3/credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCredential(credentialId, body, callback)</td>
    <td style="padding:15px">Update credential</td>
    <td style="padding:15px">{base_path}/{version}/v3/credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCredential(credentialId, callback)</td>
    <td style="padding:15px">Delete credential</td>
    <td style="padding:15px">{base_path}/{version}/v3/credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDomains(name, enabled, callback)</td>
    <td style="padding:15px">List domains</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDomain(body, callback)</td>
    <td style="padding:15px">Create domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDomainDetails(domainId, callback)</td>
    <td style="padding:15px">Show domain details</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDomain(domainId, body, callback)</td>
    <td style="padding:15px">Update domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDomain(domainId, callback)</td>
    <td style="padding:15px">Delete domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDefaultConfigurationSettings(callback)</td>
    <td style="padding:15px">Show default configuration settings</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/config/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDefaultConfigurationForAGroup(group, callback)</td>
    <td style="padding:15px">Show default configuration for a group</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/config/{pathv1}/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDefaultOptionForAGroup(group, option, callback)</td>
    <td style="padding:15px">Show default option for a group</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/config/{pathv1}/{pathv2}/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDomainGroupOptionConfiguration(domainId, group, option, callback)</td>
    <td style="padding:15px">Show domain group option configuration</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/config/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDomainGroupOptionConfiguration(domainId, group, option, body, callback)</td>
    <td style="padding:15px">Update domain group option configuration</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/config/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDomainGroupOptionConfiguration(domainId, group, option, callback)</td>
    <td style="padding:15px">Delete domain group option configuration</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/config/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDomainGroupConfiguration(domainId, group, callback)</td>
    <td style="padding:15px">Show domain group configuration</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/config/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDomainGroupConfiguration(domainId, group, body, callback)</td>
    <td style="padding:15px">Update domain group configuration</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/config/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDomainGroupConfiguration(domainId, group, callback)</td>
    <td style="padding:15px">Delete domain group configuration</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/config/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDomainConfiguration(domainId, body, callback)</td>
    <td style="padding:15px">Create domain configuration</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDomainConfiguration(domainId, callback)</td>
    <td style="padding:15px">Show domain configuration</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDomainConfiguration(domainId, body, callback)</td>
    <td style="padding:15px">Update domain configuration</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDomainConfiguration(domainId, callback)</td>
    <td style="padding:15px">Delete domain configuration</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroups(name, domainId, callback)</td>
    <td style="padding:15px">List groups</td>
    <td style="padding:15px">{base_path}/{version}/v3/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroup(body, callback)</td>
    <td style="padding:15px">Create group</td>
    <td style="padding:15px">{base_path}/{version}/v3/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showGroupDetails(groupId, callback)</td>
    <td style="padding:15px">Show group details</td>
    <td style="padding:15px">{base_path}/{version}/v3/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroup(groupId, body, callback)</td>
    <td style="padding:15px">Update group</td>
    <td style="padding:15px">{base_path}/{version}/v3/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroup(groupId, callback)</td>
    <td style="padding:15px">Delete group</td>
    <td style="padding:15px">{base_path}/{version}/v3/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUsersInGroup(passwordExpiresAt, groupId, callback)</td>
    <td style="padding:15px">List users in group</td>
    <td style="padding:15px">{base_path}/{version}/v3/groups/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserToGroup(userId, groupId, callback)</td>
    <td style="padding:15px">Add user to group</td>
    <td style="padding:15px">{base_path}/{version}/v3/groups/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkWhetherUserBelongsToGroup(userId, groupId, callback)</td>
    <td style="padding:15px">Check whether user belongs to group</td>
    <td style="padding:15px">{base_path}/{version}/v3/groups/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeUserFromGroup(userId, groupId, callback)</td>
    <td style="padding:15px">Remove user from group</td>
    <td style="padding:15px">{base_path}/{version}/v3/groups/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignRoleToUserOnProjectsOwnedByDomain(domainId, roleId, userId, callback)</td>
    <td style="padding:15px">Assign role to user on projects owned by domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/OS-INHERIT/domains/{pathv1}/users/{pathv2}/roles/{pathv3}/inherited_to_projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkIfUserHasAnInheritedProjectRoleOnDomain(domainId, roleId, userId, callback)</td>
    <td style="padding:15px">Check if user has an inherited project role on domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/OS-INHERIT/domains/{pathv1}/users/{pathv2}/roles/{pathv3}/inherited_to_projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeAnInheritedProjectRoleFromUserOnDomain(domainId, roleId, userId, callback)</td>
    <td style="padding:15px">Revoke an inherited project role from user on domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/OS-INHERIT/domains/{pathv1}/users/{pathv2}/roles/{pathv3}/inherited_to_projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignRoleToGroupOnProjectsOwnedByADomain(domainId, groupId, roleId, callback)</td>
    <td style="padding:15px">Assign role to group on projects owned by a domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/OS-INHERIT/domains/{pathv1}/groups/{pathv2}/roles/{pathv3}/inherited_to_projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkIfGroupHasAnInheritedProjectRoleOnDomain(domainId, groupId, roleId, callback)</td>
    <td style="padding:15px">Check if group has an inherited project role on domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/OS-INHERIT/domains/{pathv1}/groups/{pathv2}/roles/{pathv3}/inherited_to_projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeAnInheritedProjectRoleFromGroupOnDomain(domainId, groupId, roleId, callback)</td>
    <td style="padding:15px">Revoke an inherited project role from group on domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/OS-INHERIT/domains/{pathv1}/groups/{pathv2}/roles/{pathv3}/inherited_to_projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserSInheritedProjectRolesOnADomain(domainId, userId, callback)</td>
    <td style="padding:15px">List user’s inherited project roles on a domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/OS-INHERIT/domains/{pathv1}/users/{pathv2}/roles/inherited_to_projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroupSInheritedProjectRolesOnDomain(domainId, groupId, callback)</td>
    <td style="padding:15px">List group’s inherited project roles on domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/OS-INHERIT/domains/{pathv1}/groups/{pathv2}/roles/inherited_to_projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignRoleToUserOnProjectsInASubtree(projectId, roleId, userId, callback)</td>
    <td style="padding:15px">Assign role to user on projects in a subtree</td>
    <td style="padding:15px">{base_path}/{version}/v3/OS-INHERIT/projects/{pathv1}/users/{pathv2}/roles/{pathv3}/inherited_to_projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkIfUserHasAnInheritedProjectRoleOnProject(projectId, roleId, userId, callback)</td>
    <td style="padding:15px">Check if user has an inherited project role on project</td>
    <td style="padding:15px">{base_path}/{version}/v3/OS-INHERIT/projects/{pathv1}/users/{pathv2}/roles/{pathv3}/inherited_to_projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeAnInheritedProjectRoleFromUserOnProject(projectId, roleId, userId, callback)</td>
    <td style="padding:15px">Revoke an inherited project role from user on project</td>
    <td style="padding:15px">{base_path}/{version}/v3/OS-INHERIT/projects/{pathv1}/users/{pathv2}/roles/{pathv3}/inherited_to_projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignRoleToGroupOnProjectsInASubtree(groupId, projectId, roleId, callback)</td>
    <td style="padding:15px">Assign role to group on projects in a subtree</td>
    <td style="padding:15px">{base_path}/{version}/v3/OS-INHERIT/projects/{pathv1}/groups/{pathv2}/roles/{pathv3}/inherited_to_projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkIfGroupHasAnInheritedProjectRoleOnProject(groupId, projectId, roleId, callback)</td>
    <td style="padding:15px">Check if group has an inherited project role on project</td>
    <td style="padding:15px">{base_path}/{version}/v3/OS-INHERIT/projects/{pathv1}/groups/{pathv2}/roles/{pathv3}/inherited_to_projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeAnInheritedProjectRoleFromGroupOnProject(groupId, projectId, roleId, callback)</td>
    <td style="padding:15px">Revoke an inherited project role from group on project</td>
    <td style="padding:15px">{base_path}/{version}/v3/OS-INHERIT/projects/{pathv1}/groups/{pathv2}/roles/{pathv3}/inherited_to_projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRoleAssignments(effective, includeNames, includeSubtree, groupId, roleId, scopeDomainId, scopeOSINHERITInheritedTo, scopeProjectId, userId, callback)</td>
    <td style="padding:15px">List role assignments</td>
    <td style="padding:15px">{base_path}/{version}/v3/role_assignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRevokedTokens(callback)</td>
    <td style="padding:15px">List revoked tokens</td>
    <td style="padding:15px">{base_path}/{version}/v3/auth/tokens/OS-PKI/revoked?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicy(body, callback)</td>
    <td style="padding:15px">Create policy</td>
    <td style="padding:15px">{base_path}/{version}/v3/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicies(type, callback)</td>
    <td style="padding:15px">List policies</td>
    <td style="padding:15px">{base_path}/{version}/v3/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showPolicyDetails(policyId, callback)</td>
    <td style="padding:15px">Show policy details</td>
    <td style="padding:15px">{base_path}/{version}/v3/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicy(policyId, body, callback)</td>
    <td style="padding:15px">Update policy</td>
    <td style="padding:15px">{base_path}/{version}/v3/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicy(policyId, callback)</td>
    <td style="padding:15px">Delete policy</td>
    <td style="padding:15px">{base_path}/{version}/v3/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProjects(domainId, enabled, isDomain, name, parentId, callback)</td>
    <td style="padding:15px">List projects</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createProject(body, callback)</td>
    <td style="padding:15px">Create project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showProjectDetails(parentsAsList, subtreeAsList, parentsAsIds, subtreeAsIds, includeLimits, projectId, callback)</td>
    <td style="padding:15px">Show project details</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateProject(projectId, body, callback)</td>
    <td style="padding:15px">Update project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProject(projectId, callback)</td>
    <td style="padding:15px">Delete project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTagsForAProject(projectId, callback)</td>
    <td style="padding:15px">List tags for a project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyTagListForAProject(projectId, body, callback)</td>
    <td style="padding:15px">Modify tag list for a project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeAllTagsFromAProject(projectId, callback)</td>
    <td style="padding:15px">Remove all tags from a project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkIfProjectContainsTag(projectId, tag, callback)</td>
    <td style="padding:15px">Check if project contains tag</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSingleTagToAProject(projectId, tag, callback)</td>
    <td style="padding:15px">Add single tag to a project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSingleTagFromProject(projectId, tag, callback)</td>
    <td style="padding:15px">Delete single tag from project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showRegionDetails(regionId, callback)</td>
    <td style="padding:15px">Show region details</td>
    <td style="padding:15px">{base_path}/{version}/v3/regions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRegion(regionId, body, callback)</td>
    <td style="padding:15px">Update region</td>
    <td style="padding:15px">{base_path}/{version}/v3/regions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRegion(regionId, callback)</td>
    <td style="padding:15px">Delete region</td>
    <td style="padding:15px">{base_path}/{version}/v3/regions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRegions(parentRegionId, callback)</td>
    <td style="padding:15px">List regions</td>
    <td style="padding:15px">{base_path}/{version}/v3/regions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRegion(body, callback)</td>
    <td style="padding:15px">Create region</td>
    <td style="padding:15px">{base_path}/{version}/v3/regions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRoles(name, domainId, callback)</td>
    <td style="padding:15px">List roles</td>
    <td style="padding:15px">{base_path}/{version}/v3/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRole(body, callback)</td>
    <td style="padding:15px">Create role</td>
    <td style="padding:15px">{base_path}/{version}/v3/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showRoleDetails(roleId, callback)</td>
    <td style="padding:15px">Show role details</td>
    <td style="padding:15px">{base_path}/{version}/v3/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRole(roleId, body, callback)</td>
    <td style="padding:15px">Update role</td>
    <td style="padding:15px">{base_path}/{version}/v3/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRole(roleId, callback)</td>
    <td style="padding:15px">Delete role</td>
    <td style="padding:15px">{base_path}/{version}/v3/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRoleAssignmentsForGroupOnDomain(domainId, groupId, callback)</td>
    <td style="padding:15px">List role assignments for group on domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/groups/{pathv2}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignRoleToGroupOnDomain(domainId, groupId, roleId, callback)</td>
    <td style="padding:15px">Assign role to group on domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/groups/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkWhetherGroupHasRoleAssignmentOnDomain(domainId, groupId, roleId, callback)</td>
    <td style="padding:15px">Check whether group has role assignment on domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/groups/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignRoleFromGroupOnDomain(domainId, groupId, roleId, callback)</td>
    <td style="padding:15px">Unassign role from group on domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/groups/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRoleAssignmentsForUserOnDomain(domainId, userId, callback)</td>
    <td style="padding:15px">List role assignments for user on domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/users/{pathv2}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignRoleToUserOnDomain(domainId, userId, roleId, callback)</td>
    <td style="padding:15px">Assign role to user on domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/users/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkWhetherUserHasRoleAssignmentOnDomain(domainId, userId, roleId, callback)</td>
    <td style="padding:15px">Check whether user has role assignment on domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/users/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignsRoleFromUserOnDomain(domainId, userId, roleId, callback)</td>
    <td style="padding:15px">Unassigns role from user on domain</td>
    <td style="padding:15px">{base_path}/{version}/v3/domains/{pathv1}/users/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRoleAssignmentsForGroupOnProject(projectId, groupId, callback)</td>
    <td style="padding:15px">List role assignments for group on project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}/groups/{pathv2}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignRoleToGroupOnProject(projectId, groupId, roleId, callback)</td>
    <td style="padding:15px">Assign role to group on project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}/groups/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkWhetherGroupHasRoleAssignmentOnProject(projectId, groupId, roleId, callback)</td>
    <td style="padding:15px">Check whether group has role assignment on project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}/groups/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignRoleFromGroupOnProject(projectId, groupId, roleId, callback)</td>
    <td style="padding:15px">Unassign role from group on project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}/groups/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRoleAssignmentsForUserOnProject(projectId, userId, callback)</td>
    <td style="padding:15px">List role assignments for user on project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}/users/{pathv2}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignRoleToUserOnProject(projectId, userId, roleId, callback)</td>
    <td style="padding:15px">Assign role to user on project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}/users/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkWhetherUserHasRoleAssignmentOnProject(projectId, userId, roleId, callback)</td>
    <td style="padding:15px">Check whether user has role assignment on project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}/users/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignRoleFromUserOnProject(projectId, userId, roleId, callback)</td>
    <td style="padding:15px">Unassign role from user on project</td>
    <td style="padding:15px">{base_path}/{version}/v3/projects/{pathv1}/users/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listImpliedInferenceRolesForRole(priorRoleId, callback)</td>
    <td style="padding:15px">List implied (inference) roles for role</td>
    <td style="padding:15px">{base_path}/{version}/v3/roles/{pathv1}/implies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRoleInferenceRule(priorRoleId, impliesRoleId, callback)</td>
    <td style="padding:15px">Create role inference rule</td>
    <td style="padding:15px">{base_path}/{version}/v3/roles/{pathv1}/implies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoleInferenceRule(priorRoleId, impliesRoleId, callback)</td>
    <td style="padding:15px">Get role inference rule</td>
    <td style="padding:15px">{base_path}/{version}/v3/roles/{pathv1}/implies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">confirmRoleInferenceRule(priorRoleId, impliesRoleId, callback)</td>
    <td style="padding:15px">Confirm role inference rule</td>
    <td style="padding:15px">{base_path}/{version}/v3/roles/{pathv1}/implies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRoleInferenceRule(priorRoleId, impliesRoleId, callback)</td>
    <td style="padding:15px">Delete role inference rule</td>
    <td style="padding:15px">{base_path}/{version}/v3/roles/{pathv1}/implies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllRoleInferenceRules(callback)</td>
    <td style="padding:15px">List all role inference rules</td>
    <td style="padding:15px">{base_path}/{version}/v3/role_inferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSystemRoleAssignmentsForAUser(userId, callback)</td>
    <td style="padding:15px">List system role assignments for a user</td>
    <td style="padding:15px">{base_path}/{version}/v3/system/users/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignASystemRoleToAUser(userId, roleId, callback)</td>
    <td style="padding:15px">Assign a system role to a user</td>
    <td style="padding:15px">{base_path}/{version}/v3/system/users/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkUserForASystemRoleAssignment(userId, roleId, callback)</td>
    <td style="padding:15px">Check user for a system role assignment</td>
    <td style="padding:15px">{base_path}/{version}/v3/system/users/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemRoleAssignmentForAUser(userId, roleId, callback)</td>
    <td style="padding:15px">Get system role assignment for a user</td>
    <td style="padding:15px">{base_path}/{version}/v3/system/users/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteASystemRoleAssignmentFromAUser(userId, roleId, callback)</td>
    <td style="padding:15px">Delete a system role assignment from a user</td>
    <td style="padding:15px">{base_path}/{version}/v3/system/users/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSystemRoleAssignmentsForAGroup(groupId, callback)</td>
    <td style="padding:15px">List system role assignments for a group</td>
    <td style="padding:15px">{base_path}/{version}/v3/system/groups/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignASystemRoleToAGroup(groupId, roleId, callback)</td>
    <td style="padding:15px">Assign a system role to a group</td>
    <td style="padding:15px">{base_path}/{version}/v3/system/groups/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkGroupForASystemRoleAssignment(groupId, roleId, callback)</td>
    <td style="padding:15px">Check group for a system role assignment</td>
    <td style="padding:15px">{base_path}/{version}/v3/system/groups/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemRoleAssignmentForAGroup(groupId, roleId, callback)</td>
    <td style="padding:15px">Get system role assignment for a group</td>
    <td style="padding:15px">{base_path}/{version}/v3/system/groups/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteASystemRoleAssignmentFromAGroup(groupId, roleId, callback)</td>
    <td style="padding:15px">Delete a system role assignment from a group</td>
    <td style="padding:15px">{base_path}/{version}/v3/system/groups/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listServices(name, type, callback)</td>
    <td style="padding:15px">List services</td>
    <td style="padding:15px">{base_path}/{version}/v3/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createService(body, callback)</td>
    <td style="padding:15px">Create service</td>
    <td style="padding:15px">{base_path}/{version}/v3/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showServiceDetails(serviceId, callback)</td>
    <td style="padding:15px">Show service details</td>
    <td style="padding:15px">{base_path}/{version}/v3/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateService(serviceId, body, callback)</td>
    <td style="padding:15px">Update service</td>
    <td style="padding:15px">{base_path}/{version}/v3/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteService(serviceId, callback)</td>
    <td style="padding:15px">Delete service</td>
    <td style="padding:15px">{base_path}/{version}/v3/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listEndpoints(interfaceParam, serviceId, regionId, callback)</td>
    <td style="padding:15px">List endpoints</td>
    <td style="padding:15px">{base_path}/{version}/v3/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEndpoint(body, callback)</td>
    <td style="padding:15px">Create endpoint</td>
    <td style="padding:15px">{base_path}/{version}/v3/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showEndpointDetails(endpointId, callback)</td>
    <td style="padding:15px">Show endpoint details</td>
    <td style="padding:15px">{base_path}/{version}/v3/endpoints/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateEndpoint(endpointId, body, callback)</td>
    <td style="padding:15px">Update endpoint</td>
    <td style="padding:15px">{base_path}/{version}/v3/endpoints/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEndpoint(endpointId, callback)</td>
    <td style="padding:15px">Delete endpoint</td>
    <td style="padding:15px">{base_path}/{version}/v3/endpoints/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRegisteredLimits(serviceId, regionId, resourceName, callback)</td>
    <td style="padding:15px">List Registered Limits</td>
    <td style="padding:15px">{base_path}/{version}/v3/registered_limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRegisteredLimits(body, callback)</td>
    <td style="padding:15px">Create Registered Limits</td>
    <td style="padding:15px">{base_path}/{version}/v3/registered_limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRegisteredLimit(registeredLimitId, body, callback)</td>
    <td style="padding:15px">Update Registered Limit</td>
    <td style="padding:15px">{base_path}/{version}/v3/registered_limits/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showRegisteredLimitDetails(registeredLimitId, callback)</td>
    <td style="padding:15px">Show Registered Limit Details</td>
    <td style="padding:15px">{base_path}/{version}/v3/registered_limits/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRegisteredLimit(registeredLimitId, callback)</td>
    <td style="padding:15px">Delete Registered Limit</td>
    <td style="padding:15px">{base_path}/{version}/v3/registered_limits/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEnforcementModel(callback)</td>
    <td style="padding:15px">Get Enforcement Model</td>
    <td style="padding:15px">{base_path}/{version}/v3/limits/model?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLimits(serviceId, regionId, resourceName, projectId, domainId, callback)</td>
    <td style="padding:15px">List Limits</td>
    <td style="padding:15px">{base_path}/{version}/v3/limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLimits(body, callback)</td>
    <td style="padding:15px">Create Limits</td>
    <td style="padding:15px">{base_path}/{version}/v3/limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLimit(limitId, body, callback)</td>
    <td style="padding:15px">Update Limit</td>
    <td style="padding:15px">{base_path}/{version}/v3/limits/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showLimitDetails(limitId, callback)</td>
    <td style="padding:15px">Show Limit Details</td>
    <td style="padding:15px">{base_path}/{version}/v3/limits/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLimit(limitId, callback)</td>
    <td style="padding:15px">Delete Limit</td>
    <td style="padding:15px">{base_path}/{version}/v3/limits/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUsers(domainId, enabled, idpId, name, passwordExpiresAt, protocolId, uniqueId, callback)</td>
    <td style="padding:15px">List users</td>
    <td style="padding:15px">{base_path}/{version}/v3/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, callback)</td>
    <td style="padding:15px">Create user</td>
    <td style="padding:15px">{base_path}/{version}/v3/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showUserDetails(userId, callback)</td>
    <td style="padding:15px">Show user details</td>
    <td style="padding:15px">{base_path}/{version}/v3/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUser(userId, body, callback)</td>
    <td style="padding:15px">Update user</td>
    <td style="padding:15px">{base_path}/{version}/v3/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(userId, callback)</td>
    <td style="padding:15px">Delete user</td>
    <td style="padding:15px">{base_path}/{version}/v3/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroupsToWhichAUserBelongs(userId, callback)</td>
    <td style="padding:15px">List groups to which a user belongs</td>
    <td style="padding:15px">{base_path}/{version}/v3/users/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProjectsForUser(userId, callback)</td>
    <td style="padding:15px">List projects for user</td>
    <td style="padding:15px">{base_path}/{version}/v3/users/{pathv1}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePasswordForUser(userId, body, callback)</td>
    <td style="padding:15px">Change password for user</td>
    <td style="padding:15px">{base_path}/{version}/v3/users/{pathv1}/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
