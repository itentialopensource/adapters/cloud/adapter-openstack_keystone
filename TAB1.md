# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Openstack_keystone System. The API that was used to build the adapter for Openstack_keystone is usually available in the report directory of this adapter. The adapter utilizes the Openstack_keystone API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The OpenStack Keystone adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Keystone, an OpenStack identity service. With this adapter you have the ability to perform operations such as:

- Add, update, manage and remove Security Policies.
- Access and manage credentials.
- Manage users, groups, and projects.

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
