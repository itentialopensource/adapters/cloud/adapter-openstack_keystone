# OpenStack Keystone

Vendor: OpenStack
Homepage:https://www.openstack.org/

Product: Keystone (an OpenStack identity service)
Product Page: https://docs.openstack.org/keystone/latest/

## Introduction
We classify OpenStack Keystone into the Cloud domain as Keystone provides authentication and authorization services for other OpenStack services in a cloud computing environmet.

## Why Integrate
The OpenStack Keystone adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Keystone, an OpenStack identity service. With this adapter you have the ability to perform operations such as:

- Add, update, manage and remove Security Policies.
- Access and manage credentials.
- Manage users, groups, and projects.

## Additional Product Documentation
The [API documents for OpenStack Keystone](https://docs.openstack.org/api-ref/identity/v3/)

